# Toolbox

Toolbox is a extensible toy project based on Java + Spring-Boot. It is used as a throughout example in [exercises and labs in DevOps lectures](https://gitlab.com/rwoerzbe/exercises/-/blob/master/sm.asciidoc).

## Dependencies

A Java Runtime Environment of version 21 or higher must be installed and the environment variable `JAVA_HOME` must be set with its path.

## Installation

### Local installation

Clone this repository start Toolbox locally with

    ./mvnw spring-boot:run

## Usage

Aim a browser at http://localhost:8080

## Test

Execute extended tests (including Selenium test) with

    ./mvnw -P autotest clean verify

## Tools

These are the tools and what they are good for.

### Todolist

A very simple CRUD application that implements a single todo list. It demonstrates how Thymeleaf templates, Spring controllers, services, repositories, and a H2 or Google Cloud PostgreSQL database play together.

Furthermore it shows how test automation with JUnit, Mockito, and Selenium works.

### Palindrome

Simple palindrome checker that logs check requests in the database.

This tool has certain omissions in the code that are supposed to be filled by students.

### Fileserver

Just serves files beneath the current director under `http://localhost:8080/files`. This is useful to make links in lab instruction
to files that are being generated beneath `target` like http://localhost:8080/files/target/site/jacoco/de.thk.rwoerzbe.toolbox.palindrome/PalindromeService.html

### Chat

Demonstrator for how a reactive Chat application can be implemented with STOMP over WebSockets.

### Quotestream

Demonstrator for Server-sent Events (SSE).

### Profile

Demonstrator for how Toolbox can act as a client in an OAuth2/OIDC authentication flow. The `/profile` page just outputs what is in the JWT token that authenticates a user's Google identity and authorizes the scopes `email`, `profile`, and `openid`.

Note that you need to configure a OAuth2 consent screen and client credentials (ID and secret) in the Google Cloud. 

1. Choose or create a Google Cloud project in https://console.cloud.google.com
1. Configure Consent Screen on https://console.cloud.google.com/apis/credentials/consent
    1. External
        1. App name = toolbox
        1. User support email = your.name@gmail.com
        1. Developer contact information = your.name@gmail.com
        1. Save and Continue
    1. Scopes: Just "Save and Continue"
    1. Test Users: none
    1. Summary: Just go Back to Dashboard
1. Configure Credentials https://console.cloud.google.com/apis/credentials
    1. + Create Credentials with OAuth Client ID
        1. Application Type = Web Application
        1. Name = toolbox
        1. Authorized redirect URIs = 
            http://localhost:8080/login/oauth2/code/google 
            any URL behind which Toolbox may be deployed to like https://toolbox-production-kxanovlm5q-ez.a.run.app/login/oauth2/code/google (must end with /login/oauth2/code/google )
            https://openidconnect.net/callback
1. Copy Client ID and Client secret below or start up toolbox like so

```
./mvnw spring-boot:run -Dspring-boot.run.arguments="--spring.security.oauth2.client.registration.google.client-id=your_client_id --spring.security.oauth2.client.registration.google.client-secret=your_client_secret"
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
