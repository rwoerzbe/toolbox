## [Unreleased]

## [1.1.0] - 2023-01-02

### Added

- Implemented PalindromeService.isPalindrome(String)
- Added Javadoc comments to TodoListService.java

## [1.0.0] - 2023-01-01

### Added

- Initial release of Toolbox