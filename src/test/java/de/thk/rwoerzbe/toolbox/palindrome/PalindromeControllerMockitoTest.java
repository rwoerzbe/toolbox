package de.thk.rwoerzbe.toolbox.palindrome;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@WebMvcTest(PalindromeController.class)
// skip securityFilterChain (which would cause http 403)
@AutoConfigureMockMvc(addFilters = false)
class PalindromeControllerMockitoTest {

    @MockBean
    PalindromeService palindromeService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void submitForm_ValidPalindrome_PageContainsResultTrue() throws Exception {
        // TODO implement test. Use comments as a guideline

        final var PALINDROME = "racecar";
        // 1 fixture setup: mock isPalindrome(PALINDROME) == true

        // 2 exercise subject under test (SUT)
        ResultActions resultActions = mockMvc.perform(post("/palindrome")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("palindromecandidate=" + PALINDROME));

        // 3 result verification
        // 3.1 Is HTTP status code equal to 200 (OK)?

        // 3.2 Check if "true" is being contained at the right place in the HTML
        // resultActions.andExpect(xpath("//*[@id=\"result\"]").string("true"));
        
    }

    @Test
    void submitForm_Nonpalindrome_PageContainsResultFalse() throws Exception {
        // TODO implement test
    }

}
