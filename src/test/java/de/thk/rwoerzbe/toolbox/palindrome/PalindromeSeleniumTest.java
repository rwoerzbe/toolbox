package de.thk.rwoerzbe.toolbox.palindrome;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.thk.rwoerzbe.toolbox.AbstractToolboxSeleniumTest;

class PalindromeSeleniumTest extends AbstractToolboxSeleniumTest {

    private final String PALINDROME_URL = getBaseUrl() + "/palindrome";

    @Test
    void palindrome_SendPalindrome_ResultTrue() throws InterruptedException {
        // TODO implement test. Use comments as guidelines

        // 2 Exercise SUT

        // 3 Result Verification
        // Check if the element with id "result" contains the text "true".

    }

    @Test
    void palindrome_SendPalindrome_ResultFalse() throws InterruptedException {
        // TODO implement test
        
    }

}
