const stompClient = new StompJs.Client({
    // insecure ws does only work for localhost
    // last brokerURL segment as configured in ChatWebSocketConfigurer.registerStompEndpoints
    brokerURL: `${(location.hostname == 'localhost' ? "ws://" : "wss://")}${location.host}/chats-websocket` 
});

stompClient.activate();

stompClient.onConnect = (frame) => {
    stompClient.subscribe(`/topic/chats/${getChatId()}`, (message) => {
        appendMessage(JSON.parse(message.body));
    });
};

stompClient.onWebSocketError = (error) => {
    console.error('Error with websocket', error);
};

stompClient.onStompError = (frame) => {
    console.error('Broker reported error: ' + frame.headers['message']);
    console.error('Additional details: ' + frame.body);
};

function sendMessage() {
    stompClient.publish({
        destination: `/toolbox/chats/${getChatId()}`,
        body: JSON.stringify(
            {
                'sender': document.getElementById('sender').value,
                'content': document.getElementById('content').value
            }
        )});
    document.getElementById('content').value = "";
}   

function appendMessage(message) {
    var tr = document.createElement("tr");
    tr.innerHTML = `<td>${message.sender}</td><td>${message.timestamp}</td><td>${message.content}</td>`;
    document.getElementById("messagelist").appendChild(tr);
}

function getChatId() {
    return window.location.pathname.split("/").at(-1);
}