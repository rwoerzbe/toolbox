const eventSource = new EventSource("/quotestream-sse");

eventSource.onmessage = (event) => {
    const li = document.createElement("li");
    li.setAttribute("class", "list-group-item");
    li.textContent = event.data;
    document.getElementById("quotes").appendChild(li);
};

eventSource.onerror = (error) => {
    console.error("Error occurred:", error);
    eventSource.close();
};