package de.thk.rwoerzbe.toolbox.randomquote;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Proxy class that implements calls to a random quote service
 */
@Service
public class RandomQuoteService {

    // cache the quotes to spare the montly free API calls
    // to APINinja. After the cache reached its QUOTE_CACHE_SIZE
    // take a random entry from it instead of an API call
    private List<String> quoteCache = new ArrayList<>();
    private int QUOTE_CACHE_SIZE = 5;
    private Random rand = new Random();

    /**
     * Implements wrapper code that calls an external web service with
     * returns a random Chuck Norris quote of the day
     * @return A Chuck Norris quote
     */
    public String getRandomQuote() {
        String quote = "";
        if (quoteCache.size() < QUOTE_CACHE_SIZE) {
            URL url;
            try {
                url = new URI("https://api.api-ninjas.com/v1/chucknorris").toURL();
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("accept", "application/json");
                connection.setRequestProperty("X-Api-Key", "FGe/+3E4pAEsCJ7V5sLCJw==3xWoJ34AiewuFJta");
                InputStream responseStream = connection.getInputStream();
                ObjectMapper mapper = new ObjectMapper();
                JsonNode root = mapper.readTree(responseStream);
                quote = root.path("joke").asText();
                quoteCache.add(quote);
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            quote = quoteCache.get(rand.nextInt(quoteCache.size()));
        }
        return quote;
    }

}