package de.thk.rwoerzbe.toolbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Implements the main function of a Spring Boot application
 */
@SpringBootApplication
@EnableScheduling // for QuoteStreamService
public class ToolboxApplication {

	/**
	 * The main method of the Spring Boot application
	 * @param args Arguments handed to the Spring Boot application
	 */
	public static void main(String[] args) {
		SpringApplication.run(ToolboxApplication.class, args);
	}

}