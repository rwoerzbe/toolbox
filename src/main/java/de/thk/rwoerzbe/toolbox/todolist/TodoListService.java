package de.thk.rwoerzbe.toolbox.todolist;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A service class that implement the logic for mananging a todolist
 */
@Service
class TodoListService {

    @Autowired
    private TodoListRepository todoListRepository;
    
    /**
     * @return All stored todos
     */
    Iterable<Todo> getAllTodos() {
        return todoListRepository.findAll();
    }

    /**
     * Creates a new todo list item
     * @param newTodo The new todo's data
     * @return the new todo with an ID
     */
    Todo createTodo(Todo newTodo) {
        return todoListRepository.save(newTodo);
    }

    /**
     * Deletes an existing todo
     * @param id The todos ID
     */
    void deleteTodo(UUID id) {
        todoListRepository.deleteById(id);
    }
    
}