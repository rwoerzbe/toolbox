package de.thk.rwoerzbe.toolbox.todolist;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Standard interface for storing and retriving {@link de.thk.rwoerzbe.toolbox.todolist.Todo}
 */
@Repository
interface TodoListRepository extends CrudRepository<Todo, UUID> {
    
}