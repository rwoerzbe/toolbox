package de.thk.rwoerzbe.toolbox.profile;

import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProfileController {

    /**
     * <code>/profile</code> needs authentication anyway as configured in
     * {@link de.thk.rwoerzbe.toolbox.config.SecurityConfig#securityFilterChain(org.springframework.security.config.annotation.web.builders.HttpSecurity)}
     * so <code>authentication</code> will be non-null and we can put some of the
     * contents of the authentication token in the model for rendering.
     * 
     * @param model          placehoders for the profile.html Thymeleaf template
     * @param authentication the parsed JWT token
     * @return "profile" that redirects to the profile.html Thymeleaf template
     */
    @GetMapping("/profile")
    public String profile(Model model, OAuth2AuthenticationToken authentication) {
        OAuth2User principal = authentication.getPrincipal();
        model.addAttribute("userName", principal.getAttribute("name"));
        model.addAttribute("userEmail", principal.getAttribute("email"));
        model.addAttribute("userPicture", principal.getAttribute("picture"));
        return "profile";
    }

}
