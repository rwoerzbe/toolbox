package de.thk.rwoerzbe.toolbox.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

/**
 * Configures the web page /profile to be a protected source. Only authenticated
 * users have access to that resource.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        // just /profile needs OAuth2 login
        return http
                // turn of CSFR checks. Otherwise POST requests to unprotected resouces get a
                // 403 forbidden
                .csrf(csrfCustomizer -> csrfCustomizer.disable())
                // avoid sticky sessions and JSESSIONID if not necessary
                .sessionManagement(authorizeHttpRequestsCustomizer -> authorizeHttpRequestsCustomizer
                        .sessionCreationPolicy(SessionCreationPolicy.NEVER))
                .authorizeHttpRequests(authorizeRequests -> authorizeRequests
                        // require authentication just for /profile
                        .requestMatchers("/profile").authenticated()
                        // but permit unauthenticated requests to all other resources
                        .anyRequest().permitAll())
                // authentication via OAuth2
                .oauth2Login(Customizer.withDefaults())
		.headers(headersCustomizer -> headersCustomizer
                        // Disable X-Frame-Options header
                        .frameOptions(frameOptions -> frameOptions.disable()))  
		.build();
    }
}