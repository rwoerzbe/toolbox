package de.thk.rwoerzbe.toolbox.chat;

import java.sql.Timestamp;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class ChatService {

    @Autowired
    ChatRepository chatRepository;

    @Autowired
    ChatMessageRepository messageRepository;
    
    Iterable<Chat> getAllChats() {
        return chatRepository.findAll();
    }

    Chat getChat(UUID id) {
        return chatRepository.findById(id).get();
    }

    void createChat(String topic) {
        Chat chat = new Chat();
        chat.setTopic(topic);
        chatRepository.save(chat);
    }

    void deleteChat(UUID id) {
        chatRepository.deleteById(id);
    }

    ChatMessage createMessage(UUID id, String sender, String content) {
        ChatMessage message = new ChatMessage();
        message.setContent(content);
        message.setSender(sender);
        message.setTimestamp(new Timestamp(System.currentTimeMillis()));
        message = messageRepository.save(message);
        Chat chat = getChat(id);
        chat.getMessages().add(message);
        chatRepository.save(chat);
        return message;
    }
}
