package de.thk.rwoerzbe.toolbox.chat;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ChatRepository extends CrudRepository<Chat, UUID> {

}
