package de.thk.rwoerzbe.toolbox.chat;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

interface ChatMessageRepository extends CrudRepository<ChatMessage, UUID> {
    
}