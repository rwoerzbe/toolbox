package de.thk.rwoerzbe.toolbox.chat;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;

@Entity
public class Chat extends BaseEntity {

    @OneToMany(fetch = FetchType.EAGER)
    private List<ChatMessage> messages;

    public List<ChatMessage> getMessages() {
        return messages;
    }

    private String topic;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

}
