package de.thk.rwoerzbe.toolbox.chat;

import java.sql.Timestamp;

import jakarta.persistence.Entity;

@Entity
class ChatMessage extends BaseEntity {

    private String sender;

    public String getSender() {
        return sender;
    }

    void setSender(String sender) {
        this.sender = sender;
    }

    private String content;

    public String getContent() {
        return content;
    }

    void setContent(String content) {
        this.content = content;
    }

    private Timestamp timestamp;

    public Timestamp getTimestamp() {
        return timestamp;
    }

    void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

}
