package de.thk.rwoerzbe.toolbox.chat;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ChatController {

    @Autowired
    private ChatService chatService;

    @GetMapping("/chats")
    public String chats(Model model) {
        model.addAttribute("chats", chatService.getAllChats());
        return "chats";
    }

    @PostMapping(value = "/chats", consumes = "application/x-www-form-urlencoded")
    public String createChat(String topic) {
        chatService.createChat(topic);
        return "redirect:/chats";
    }

    @DeleteMapping("/chats/{id}")
    public String deleteChat(@PathVariable String id) {
        chatService.deleteChat(UUID.fromString(id));
        return "redirect:/chats";
    }

    @GetMapping("chats/{id}")
    public String chat(Model model, @PathVariable String id) {
        model.addAttribute("chat", chatService.getChat(UUID.fromString(id)));
        return "chat";
    }

    // Configure path for publications of chat messages, i.e. on the client side
    // stompClient.publish({"destination": "/toolbox/chats/{id}" ...})
    // That /toolbox prefix is configured in ChatWebSocketConfigurer.configureMessageBroker
    @MessageMapping("/chats/{id}") 
    // Configure path for subscriptions to chat messages
    // i.e. stompClient.subscribe({"/topics/chats/{id}"...
    @SendTo("/topic/chats/{id}") 
    public ChatMessage broadcastMessage(@DestinationVariable String id, ChatMessage message) {
        // Here we could implement some transformation logic for messages
        return chatService.createMessage(UUID.fromString(id), message.getSender(), message.getContent());
    }
}
 