package de.thk.rwoerzbe.toolbox.quotestream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
public class QuoteStreamRestController {

    @Autowired
    QuoteStreamService quoteStreamService;

    /**
     * Implements a server-sent events endpoint that streams quotes as text
     * 
     * @return a
     *         {@link org.springframework.web.servlet.mvc.method.annotation.SseEmitter}
     *         that
     *         is later used to send server-sent events (text messages in this case)
     *         to a client.
     * @see <a href=
     *      "https://docs.spring.io/spring-framework/reference/web/webmvc/mvc-ann-async.html#mvc-ann-async-sse">Spring
     *      Doc about SSE</a>
     */
    @GetMapping(path = "/quotestream-sse", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter subscribe() {
        SseEmitter emitter = new SseEmitter(0L);
        quoteStreamService.addEmitter(emitter);
        return emitter;
    }

}
