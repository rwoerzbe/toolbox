package de.thk.rwoerzbe.toolbox.quotestream;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import de.thk.rwoerzbe.toolbox.randomquote.RandomQuoteService;

@Service
class QuoteStreamService {
    @Autowired
    private RandomQuoteService randomQuoteService;

    // Contains all connected emmitters
    // We use a thread safe variant of a List such that
    // java.util.ConcurrentModificationException do not occur in
    // #sendQuotes()
    private final List<SseEmitter> emitters = new CopyOnWriteArrayList<>();

    /**
     * Adds a new emitter which represents a single connected client
     * 
     * @param emitter the new emitter
     */
    void addEmitter(SseEmitter emitter) {
        emitters.add(emitter);
        emitter.onCompletion(() -> emitters.remove(emitter));
        emitter.onTimeout(() -> emitters.remove(emitter));
    }

    /**
     * Sends a quote to all emitters.
     * 
     * @see de.thk.rwoerzbe.toolbox.ToolboxApplication which must have
     *      {@link org.springframework.scheduling.annotation.EnableScheduling}
     */
    @Scheduled(fixedRate = 1000)
    private void sendQuotes() {
        final String quote = randomQuoteService.getRandomQuote();
        for (SseEmitter emitter : emitters) {
            try {
                emitter.send(quote);
            } catch (IOException e) {
                emitter.complete();
                emitters.remove(emitter);
            }
        }
    }
}
