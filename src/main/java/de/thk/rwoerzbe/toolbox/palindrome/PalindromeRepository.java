package de.thk.rwoerzbe.toolbox.palindrome;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

/**
 * Standard interface for storing and receiving 
 * {@link de.thk.rwoerzbe.toolbox.palindrome.Palindrome} entities
 */
interface PalindromeRepository extends CrudRepository<Palindrome, UUID> {

}