package de.thk.rwoerzbe.toolbox.palindrome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class that implements the palindrome checker
 */
@Service
class PalindromeService {

    @Autowired
    private PalindromeRepository palindromeRepository;

    /**
     * Takes a non-null <code>candidate</code> string of length n and returns true
     * iff on the position k and n-k is the same character in the string. For
     * example,
     * 'noon' and 'racecar' are a palindromes.
     * Additionally, the palindrome candidate is stored along with the computed
     * palindrome property (true or false) for later analysis.
     * 
     * @param candidate A non-null candidate string to check.
     * @return true iff the <code>candidate</code> is a palindrome.
     * @throws IllegalArgumentException thrown iff candidate == null. No storage
     *                                  takes place then.
     */
    boolean isPalindrome(String candidate) throws IllegalArgumentException {
        throw new UnsupportedOperationException("Not implemented");
    }

}